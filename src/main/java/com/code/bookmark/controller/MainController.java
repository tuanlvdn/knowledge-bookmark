package com.code.bookmark.controller;

import com.code.bookmark.exception.ResourceNotFoundException;
import com.code.bookmark.model.Bookmark;
import com.code.bookmark.model.dto.BookmarkDto;
import com.code.bookmark.model.dto.WriteBookmarkDto;
import com.code.bookmark.repository.BookmarkRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1")
public class MainController {
    @Autowired
    private BookmarkRepository bookmarkRepository;
    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/bookmarks")
    public List<BookmarkDto> getAllBookmarks() {
        List<BookmarkDto> bookmarkDtoList = new ArrayList<>();
        List<Bookmark> bookmarkList = bookmarkRepository.findAll();
        for (Bookmark bookmark: bookmarkList) {
            bookmarkDtoList.add(modelMapper.map(bookmark,BookmarkDto.class));
        }
        return bookmarkDtoList;
    }

    @PostMapping("/bookmarks")
    public Bookmark createBookmark(@Valid @RequestBody WriteBookmarkDto bookmarkDto) {
        Bookmark bookmark = modelMapper.map(bookmarkDto, Bookmark.class);
        return  bookmarkRepository.save(bookmark);
    }

    @GetMapping("/bookmarks/{id}")
    public ResponseEntity<BookmarkDto> getBookmarkById(@PathVariable(value = "id") long id) throws ResourceNotFoundException {
        Bookmark bookmark =  bookmarkRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Bookmark not found for this id: "+id));
        BookmarkDto bookmarkDto = modelMapper.map(bookmark,BookmarkDto.class);
        return ResponseEntity.ok().body(bookmarkDto);
    }

    @PutMapping("/bookmarks/{id}")
    public ResponseEntity<Bookmark> updateBookmark(@PathVariable(value = "id") long id, @RequestBody WriteBookmarkDto bookmarkDetails) throws ResourceNotFoundException {
        Bookmark bookmark = modelMapper.map(bookmarkDetails, Bookmark.class);
        bookmark = bookmarkRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Bookmark not found for this id: "+id));
        bookmark.setUrl(bookmarkDetails.getUrl());
        bookmark.setType(bookmarkDetails.getType());
        bookmark.setTags(bookmarkDetails.getTags());
        bookmark.setNote(bookmarkDetails.getNote());
        bookmarkRepository.save(bookmark);
        return ResponseEntity.ok().body(bookmark);
    }

    @DeleteMapping("/bookmarks/{id}")
    public ResponseEntity deleteBookmark(@PathVariable(value = "id") long id) throws ResourceNotFoundException {
        bookmarkRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Bookmark not found for this id: "+id));
        bookmarkRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
