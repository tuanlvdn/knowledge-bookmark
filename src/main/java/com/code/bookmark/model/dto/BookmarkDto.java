package com.code.bookmark.model.dto;

import lombok.*;
import java.time.Instant;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class BookmarkDto {
    private long id;

    private String url;

    private String type;

    private List<String> tags;

    private String note;

    private Instant created_at;

    private Instant last_updated;
}
