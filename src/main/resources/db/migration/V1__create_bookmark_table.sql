CREATE TABLE bookmark (
  id serial PRIMARY KEY,
  url VARCHAR(512) UNIQUE NOT NULL,
  "type" VARCHAR(50),
  tags text[],
  note text,
  created_at timestamptz,
  last_updated timestamptz
);

CREATE INDEX idx_bookmark_id ON bookmark(id);
CREATE INDEX idx_bookmark_url ON bookmark(url);
CREATE INDEX idx_bookmark_tags ON bookmark USING gin(tags);