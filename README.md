- Setup: (https://start.spring.io/)
	+ Spring Boot ver2.3.0.RELEASE
	+ Group: com.code
	+ Artifact: bookmark
	+ Name: Bookmark
	+ Package name: com.code.bookmark
	+ Packaging: Jar
	+ Java 14
	+ Dependencies: Spring Web, data-jpa, flyway-core, postgresql

	Java SE 14 Archive Downloads: (https://www.oracle.com/java/technologies/javase/jdk14-archive-downloads.html)
		+ jdk-14_windows-x64_bin.exe

- Issues:
	1. Cannot run on tomcat, just display ( Process finished with exit code 0)
	Solution: Rem scope "provided" in pom.xml and reimport or remove spring-boot-starter-tomcat dependency(just need to build War pack).
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
		</dependency>

	2. No support java14
	Solution:
		Click File -> Project Structure (Project, Modules, SDKs) change jdk path.
	
	3. Delete MainController class, but still run code of MainController
	Solution:
		Delete MainController.class in (target/classes/com/code/bookmark/controller).
	
	4. Column "tags" is of type text[] but expression is of type bytea
    Solution: In Bookmark class: String[] -> List<String>,  add annotation @ElementCollection(targetClass = String.class)
        @ElementCollection(targetClass = String.class)
        @Column(name = "tags")
        private List<String> tags;


